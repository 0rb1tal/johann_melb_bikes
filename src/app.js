import {MelbDataService} from './services/melbdataservice.js';
import {BikeStation} from './classes/bikestation.js';
import {DATA} from './bikemelbdata.js';
import {DataError} from './services/error.js';

console.log('app.js started.');
var dataService = new MelbDataService();
dataService.loadData(DATA);

var sortedByNo = dataService.getStationsSortedByBikeNo();
for (let printNo of sortedByNo) {
    console.log(printNo);
    console.log('Sorting by number of bikes.');
}

var sortedByID = dataService.getStationsSortedByID();
for (let printID of sortedByID) {
    console.log(printID);
    console.log('Sorting by ID.');
}

var printError = MelbDataService.errors;
if (printError != null)
{
    console.log(printError);
}
