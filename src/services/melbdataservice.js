import {BikeStation} from '../classes/bikestation.js';
import {DataError} from './error.js';

export class MelbDataService {
    constructor() {
        this.bikestations = [];
        this.errors = [];
    }
    getStationsSortedByBikeNo() {
        return this.bikestations.sort(function(station0, station1) {
            if (station0.nbbikes < station1.nbbikes)
                return -1;
            if (station0.nbbikes > station1.nbbikes)
                return 1;
            return 0;
            }
        )
    }
    getStationsSortedByID() {
        return this.bikestations.sort(function(station0, station1) {
                if (station0.id < station1.id)
                    return -1;
                if (station0.id > station1.id)
                    return 1;
                return 0;
            }
        )
    }
    loadData(DATA) {
        for(let data of DATA) {
            switch(data.terminalname) {
                case null:
                    let e = new DataError('Invalid Terminal Name', data);
                    this.errors.push(e);
                    console.log("failed to load data");
                    break;
                default:
                    let bikeStationInstance = this.loadBikeStation(data);
                    if (bikeStationInstance) {
                        this.bikestations.push(bikeStationInstance);
                        console.log('pushed instance');
                    }
                    break;
            }
        }
    }
    loadBikeStation(bikeStationInstance) {
        try {
            let b = new BikeStation(bikeStationInstance.id, bikeStationInstance.nbbikes, bikeStationInstance.coordinates);
            return b;

        }
        catch(e) {
            this.errors.push(new DataError('error loading bike station'));
        }
    }


}