export class BikeStation {

    constructor(id, nbbikes, coordinates, terminalname) {
        this.id = id;
        this.nbbikes = nbbikes;
        this.coordinates = coordinates;
        this.terminalname = terminalname;
    }
}